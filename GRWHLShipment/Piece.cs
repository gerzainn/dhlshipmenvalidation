﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment
{
    [XmlRoot(ElementName = "Piece")]
    public class Piece
    {
        [XmlElement(ElementName = "DataIdentifier")]
        public string DataIdentifier { get; set; }
        [XmlElement(ElementName = "Depth")]
        public string Depth { get; set; }
        [XmlElement(ElementName = "DimWeight")]
        public string DimWeight { get; set; }
        [XmlElement(ElementName = "Height")]
        public string Height { get; set; }
        [XmlElement(ElementName = "LicensePlate")]
        public string LicensePlate { get; set; }
        [XmlElement(ElementName = "LicensePlateBarCode")]
        public string LicensePlateBarCode { get; set; }
        [XmlElement(ElementName = "PackageType")]
        public string PackageType { get; set; }
        [XmlElement(ElementName = "PieceNumber")]
        public string PieceNumber { get; set; }
        [XmlElement(ElementName = "Weight")]
        public string Weight { get; set; }
        [XmlElement(ElementName = "Width")]
        public string Width { get; set; }
    }
}
