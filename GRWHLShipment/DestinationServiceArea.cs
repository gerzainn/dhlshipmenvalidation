﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment
{
    [XmlRoot(ElementName = "DestinationServiceArea")]
    public class DestinationServiceArea
    {
        [XmlElement(ElementName = "FacilityCode")]
        public string FacilityCode { get; set; }
        [XmlElement(ElementName = "InboundSortCode")]
        public string InboundSortCode { get; set; }
        [XmlElement(ElementName = "ServiceAreaCode")]
        public string ServiceAreaCode { get; set; }
    }
}
