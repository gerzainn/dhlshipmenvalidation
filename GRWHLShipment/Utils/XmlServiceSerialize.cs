﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;

namespace GRWHLShipment.Utils
{
    public class XmlServiceSerialize
    {
        
        public static string Serialize(object _in)
        {
            var xmlserializer = new XmlSerializer(_in.GetType());
            var stringWriter = new StringWriter();

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("req", "http://www.dhl.com");
            ns.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");

            XmlWriterSettings settings = new XmlWriterSettings
            {
                OmitXmlDeclaration = true,
                Indent = true
            };

            using (var writer = XmlWriter.Create(stringWriter, settings))
            {
                xmlserializer.Serialize(writer, _in, ns);
                return stringWriter.ToString();
            }
        }

        public static T Deserialize<T>(string _in)
        {
            var xmlserializer = new XmlSerializer(typeof(T));

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("req", "http://www.dhl.com");
            ns.Add("xsi", "http://www.w3.org/2001/XMLSchema-instance");

            using (var reader = new StringReader(_in))
            {
                return (T)xmlserializer.Deserialize(reader);
            }
        }
    }
  }

