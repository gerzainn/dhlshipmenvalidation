﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment
{
    [XmlRoot(ElementName = "Note")]
    public class Note
    {
        [XmlElement(ElementName = "ActionNote")]
        public string ActionNote { get; set; }
    }
}
