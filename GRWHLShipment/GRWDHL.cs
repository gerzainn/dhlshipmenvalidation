﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Net;
using System.Xml;
using System.Xml.Serialization;
using GRWHLShipment.Request;

namespace GRWHLShipment
{
    public class GRWDHL
    {
        private string MessageReferenceTexto;
        private string errorMessage;
        private HttpWebRequest request;
        private HttpWebResponse response;
        private readonly string DHLURL = "https://xmlpitest-ea.dhl.com/XMLShippingServlet";
        private StreamReader reader;
        private bool futureDate = false;
        private string response_service;
        private XmlSerializer serializer;
        private string actionNoteValue;
        private string AirBillNumber;
        private XmlSerializerNamespaces serializerNamespaces;

        public GRWDHL()
        {
            serializerNamespaces = new XmlSerializerNamespaces();
        }
        /// <summary>
        ///  Permite leer el archivo xml en una ruta predefinida , el cual se usa para hacer la petición
        ///  hacia el servicio de validación de Shipment de DHL
        /// </summary>
        public void ReadXmlFile()
        {
            try
            {

                XDocument element = XDocument.Load(@"C:\Users\resen\Desktop\DHL\Petición.xml", LoadOptions.None);
                var MessageReference = from reference in element.Descendants()
                                       select reference;
                foreach (var elemento in element.Descendants("ServiceHeader"))
                {
                    MessageReferenceTexto = (string)elemento.Element("MessageReference");
                    if (MessageReferenceTexto != null)
                    {
                        CreateRequest(element.ToString());
                        GetResponse();
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                errorMessage = ex.Message;
            }
            
        }
        /// <summary>
        /// Construye la petición hacia el servicio de validacion de shipment de DHL
        /// </summary>
        /// <param name="xml"> Archivo xml para generar la petición con informacion sobre el shipment </param>
        public void CreateRequest(string xml)
        {
            try
            {
                byte[] size;
                Stream dataStream;
                size = Encoding.UTF8.GetBytes(xml);
                request = (HttpWebRequest)WebRequest.Create(DHLURL);
                request.ContentType = "application/x-www-form-urlencoded; encoding=utf-8";
                request.Method = "POST";
                request.ContentLength = size.Length;
                dataStream = request.GetRequestStream();
                dataStream.Write(size, 0, size.Length);
                dataStream.Close();
            }
            catch (WebException ex)
            {
                errorMessage = ex.Message;
            }
        }

       /// <summary>
       /// Método para obtener la respuesta de la validación de shipment de DHL en un xml string 
       /// Iniciar la respuesta en la posicion =0 para evitar que la respuesta no se leea 
       /// </summary>
        public void GetResponse()
        {
            try
            {
                using (response = request.GetResponse() as HttpWebResponse)
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
                        response_service = reader.ReadToEnd();
                        MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(response_service));
                        memoryStream.Position = 0;
                        if (response_service != null)
                        {
                                XElement element = XElement.Load(memoryStream);
                                var Actionnote = from reference in element.Descendants()
                                                    select reference;
                                foreach (var elemento in element.Descendants("Note"))
                                {

                                    actionNoteValue = (string)elemento.Element("ActionNote");


                                    if (actionNoteValue == "Success")
                                    {
                                        AirBillNumber = (string)element.Element("AirwayBillNumber");
                                        
                                        foreach (var pdf in element.Descendants("LabelImage"))
                                        {
                                            string OutputFormat = (string)pdf.Element("OutputFormat");

                                            if (OutputFormat != null)
                                            {
                                                string OutputImage = (string)pdf.Element("OutputImage");
                                                string directoriopdf = @"C:\Users\resen\Desktop\DHL\";
                                                string nombre = "Etiqueta.pdf";
                                                byte[] data = Convert.FromBase64String(OutputImage);
                                                if (Directory.Exists(directoriopdf))
                                                {
                                                    directoriopdf = directoriopdf + nombre;
                                                    using (FileStream writer = new FileStream(directoriopdf, FileMode.Create, FileAccess.Write))
                                                    {
                                                        writer.Write(data, 0, data.Length);

                                                    }
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Error al encontrar la ruta del archivo");
                                                }

                                            }
                                        }

                                    } // Termina validación respuesta de ShipmentDhl para verificar el estado sea Sucess 
                                }
                                
                        }
                    }

                }
            }
            catch (WebException ex)
            {
                errorMessage = ex.Message;
                reader.Close();
            }
        }
        /// <summary>
        ///  Crear una peticion generando el xml , para hacerlo de manera dinamica 
        ///  Nota : EL siguinte paso es obtener los datos ya sea como prueba desde una aplicacion de WindowsForms , para este caso no 
        ///  se muetren como en este método 
        /// </summary>
        public void CrearXmlPeticion()
        {
            ReqShipmentRequest shipmentrequest = new ReqShipmentRequest();
            RequestXml request = new RequestXml();
            BillingRequest billingRequest = new BillingRequest();
            billingRequest.ShipperAccountNumber = "753871175";
            billingRequest.ShippingPaymentType = "S";
            billingRequest.BillingAccountNumber = "753871175";
            billingRequest.DutyPaymentType = "S";
            billingRequest.DutyAccountNumber = "75387117";
            List<string> adressline = new List<string>();
            adressline.Add("9 Changi Business Park Central 1");
            adressline.Add("3th Floor");
            adressline.Add("The IBM Place");

            ServiceHeaderRequest serviceHeaderRequest = new ServiceHeaderRequest();
            serviceHeaderRequest.MessageTime = "2001-12-17T09:30:47-05:00";
            serviceHeaderRequest.MessageReference = "Shipmnt_AM_US_lblimg_62_sch_PLT";
            serviceHeaderRequest.SiteID = "DServiceVal";
            serviceHeaderRequest.Password = "testServVal";

            ContactRequest contactRequest = new ContactRequest();
            contactRequest.PersonName = "Mrs Orlander";
            contactRequest.PhoneNumber = "506-851-2271";
            contactRequest.PhoneExtension = "7862";
            contactRequest.FaxNumber = "506-851-7403";
            contactRequest.Telex = "506-851-7121";
            contactRequest.Email = "anc@email.com";

            ConsigneeRequest consigneeRequest = new ConsigneeRequest();
            consigneeRequest.CompanyName = "IBM Singapore Pte Ltd";
            consigneeRequest.AddressLine = adressline;
            consigneeRequest.City = "Singapore";
            consigneeRequest.PostalCode = "486048";
            consigneeRequest.CountryCode = "SG";
            consigneeRequest.CountryName = "Singapore";
            consigneeRequest.Contact = contactRequest;

            CommodityRequest commodityRequest = new CommodityRequest();
            commodityRequest.CommodityCode = "cc";
            commodityRequest.CommodityName = "cn";

            DutiableRequest dutiableRequest = new DutiableRequest
            {
                DeclaredValue = "200.00",
                DeclaredCurrency = "USD",
                ScheduleB = "3002905110",
                ExportLicense = "D123456",
                ShipperEIN = "112233445566",
                ShipperIDType = "S",
                ImportLicense = "ImportLic",
                ConsigneeEIN = "ConEIN2123",
                TermsOfTrade = "DAP"
            };

            ReferenceRequest referenceRequest = new ReferenceRequest();
            referenceRequest.ReferenceID = "AM international shipment";
            referenceRequest.ReferenceType = "St";

            PieceRequest pieceRequest = new PieceRequest
            {
                PieceID = "1",
                PackageType = "EE",
                Weight = "10.0",
                Width = "100",
                Height = "200",
                Depth = "300"
            };

            PiecesRequest piecesRequest = new PiecesRequest();
            piecesRequest.Piece = pieceRequest;




            ShipmentDetailsRequest shipmentDetailsRequest = new ShipmentDetailsRequest
            {
                Pieces = piecesRequest,
                NumberOfPieces = "1",
                Weight = "10.0",
                WeightUnit = "L",
                GlobalProductCode = "P",
                LocalProductCode = "P",
                Date = "2019-10-23",
                Contents = "AM international shipment contents",
                DoorTo = "DD",
                DimensionUnit = "I",
                InsuredAmount = "1200.00",
                PackageType = "EE",
                IsDutiable = "Y",
                CurrencyCode = "USD"
            };

            ShipperRequest shipperRequest = new ShipperRequest
            {
                ShipperID = "751008818",
                CompanyName = "IBM Corporation",
                RegisteredAccount = "751008818"
            };
            List<string> adreeslineshiper = new List<string>();
            adreeslineshiper.Add("1 New Orchard Road");
            adreeslineshiper.Add("Armonk");
            shipperRequest.AddressLine = adreeslineshiper;
            shipperRequest.City = "New York";
            shipperRequest.Division = "ny";
            shipperRequest.DivisionCode = "ny";
            shipperRequest.PostalCode = "10504";
            shipperRequest.CountryCode = "US";
            shipperRequest.CountryName = "United States Of America";

           

            request.ServiceHeader = serviceHeaderRequest;
            shipmentrequest.Billing = billingRequest;
            shipmentrequest.Consignee = consigneeRequest;
            shipmentrequest.Request = request;
            shipmentrequest.Consignee = consigneeRequest;
            shipmentrequest.Dutiable = dutiableRequest;
            shipmentrequest.Reference = referenceRequest;
            shipmentrequest.ShipmentDetails = shipmentDetailsRequest;
            shipmentrequest.RegionCode = "AM";
            shipmentrequest.RequestedPickupTime = "Y";
            shipmentrequest.LanguageCode = "en";
            shipmentrequest.PiecesEnabled = "Y";

            XmlAttributes attrs = new XmlAttributes();
            XmlAttributeOverrides xOver = new XmlAttributeOverrides();

            XmlRootAttribute xRoot = new XmlRootAttribute
            {
                Namespace = "req:http://www.dhl.com",
                ElementName = "ShipmentRequest"
            };
            attrs.XmlRoot = xRoot;
            xOver.Add(typeof(ReqShipmentRequest), attrs);
            XmlAttributes tempAttrs;
            tempAttrs = xOver[typeof(ReqShipmentRequest)];
            serializer = new XmlSerializer(typeof(ReqShipmentRequest),xOver);
            MemoryStream ms = new MemoryStream();
            serializer.Serialize(ms, shipmentrequest);
            ms.Seek(0, 0);
            StreamReader reader = new StreamReader(ms);
            string xml = reader.ReadToEnd();
        }
        

        public void GuardarResponse(string response)
        {
            string directorio = @"C:\Users\Soporte\Documents\Response\ResponseServer.xml";
            MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(response));
            XDocument doc = XDocument.Load(response);
            
            try
            {
                XmlWriterSettings settings = new XmlWriterSettings
                {
                    OmitXmlDeclaration = true,
                    Indent = true

                };
              
                using (XmlWriter writer = XmlWriter.Create(stream, settings))
                {
                    doc.Save(writer);
                }
            }
            catch (Exception ex)
            {

                if (ex is XmlException)
                {
                    Console.WriteLine(ex.Message);
                }
                   
            }
           
        }
    }
}
