﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment.Request
{
    [XmlRoot(ElementName = "Contact")]
    public class ContactRequest
    {
        [XmlElement(ElementName = "PersonName")]
        public string PersonName { get; set; }
        [XmlElement(ElementName = "PhoneNumber")]
        public string PhoneNumber { get; set; }
        [XmlElement(ElementName = "PhoneExtension")]
        public string PhoneExtension { get; set; }
        [XmlElement(ElementName = "FaxNumber")]
        public string FaxNumber { get; set; }
        [XmlElement(ElementName = "Telex")]
        public string Telex { get; set; }
        [XmlElement(ElementName = "Email")]
        public string Email { get; set; }
    }
}
