﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment.Request
{
    [XmlRoot(ElementName = "ShipmentDetails")]
    public class ShipmentDetailsRequest
    {
        [XmlElement(ElementName = "NumberOfPieces")]
        public string NumberOfPieces { get; set; }
        [XmlElement(ElementName = "Pieces")]
        public PiecesRequest Pieces { get; set; }
        [XmlElement(ElementName = "Weight")]
        public string Weight { get; set; }
        [XmlElement(ElementName = "WeightUnit")]
        public string WeightUnit { get; set; }
        [XmlElement(ElementName = "GlobalProductCode")]
        public string GlobalProductCode { get; set; }
        [XmlElement(ElementName = "LocalProductCode")]
        public string LocalProductCode { get; set; }
        [XmlElement(ElementName = "Date")]
        public string Date { get; set; }
        [XmlElement(ElementName = "Contents")]
        public string Contents { get; set; }
        [XmlElement(ElementName = "DoorTo")]
        public string DoorTo { get; set; }
        [XmlElement(ElementName = "DimensionUnit")]
        public string DimensionUnit { get; set; }
        [XmlElement(ElementName = "InsuredAmount")]
        public string InsuredAmount { get; set; }
        [XmlElement(ElementName = "PackageType")]
        public string PackageType { get; set; }
        [XmlElement(ElementName = "IsDutiable")]
        public string IsDutiable { get; set; }
        [XmlElement(ElementName = "CurrencyCode")]
        public string CurrencyCode { get; set; }
    }
}
