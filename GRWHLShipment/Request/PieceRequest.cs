﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment.Request
{
    [XmlRoot(ElementName = "Piece")]
    public class PieceRequest
    {
        [XmlElement(ElementName = "PieceID")]
        public string PieceID { get; set; }
        [XmlElement(ElementName = "PackageType")]
        public string PackageType { get; set; }
        [XmlElement(ElementName = "Weight")]
        public string Weight { get; set; }
        [XmlElement(ElementName = "DimWeight")]
        public string DimWeight { get; set; }
        [XmlElement(ElementName = "Width")]
        public string Width { get; set; }
        [XmlElement(ElementName = "Height")]
        public string Height { get; set; }
        [XmlElement(ElementName = "Depth")]
        public string Depth { get; set; }
    }
}
