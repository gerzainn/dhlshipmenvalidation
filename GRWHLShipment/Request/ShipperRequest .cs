﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment.Request
{
    [XmlRoot(ElementName = "Shipper")]
    public class ShipperRequest
    {
        [XmlElement(ElementName = "ShipperID")]
        public string ShipperID { get; set; }
        [XmlElement(ElementName = "CompanyName")]
        public string CompanyName { get; set; }
        [XmlElement(ElementName = "RegisteredAccount")]
        public string RegisteredAccount { get; set; }
        [XmlElement(ElementName = "AddressLine")]
        public List<string> AddressLine { get; set; }
        [XmlElement(ElementName = "City")]
        public string City { get; set; }
        [XmlElement(ElementName = "Division")]
        public string Division { get; set; }
        [XmlElement(ElementName = "DivisionCode")]
        public string DivisionCode { get; set; }
        [XmlElement(ElementName = "PostalCode")]
        public string PostalCode { get; set; }
        [XmlElement(ElementName = "CountryCode")]
        public string CountryCode { get; set; }
        [XmlElement(ElementName = "CountryName")]
        public string CountryName { get; set; }
        [XmlElement(ElementName = "Contact")]
        public ContactRequest Contact { get; set; }
    }
}
