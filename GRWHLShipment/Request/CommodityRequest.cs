﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment.Request
{
    [XmlRoot(ElementName = "Commodity")]
    public class CommodityRequest
    {
        [XmlElement(ElementName = "CommodityCode")]
        public string CommodityCode { get; set; }
        [XmlElement(ElementName = "CommodityName")]
        public string CommodityName { get; set; }
    }
}
