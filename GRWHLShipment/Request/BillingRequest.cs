﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment.Request
{
    [XmlRoot(ElementName = "Billing")]
    public class BillingRequest
    {
        [XmlElement(ElementName = "ShipperAccountNumber")]
        public string ShipperAccountNumber { get; set; }
        [XmlElement(ElementName = "ShippingPaymentType")]
        public string ShippingPaymentType { get; set; }
        [XmlElement(ElementName = "BillingAccountNumber")]
        public string BillingAccountNumber { get; set; }
        [XmlElement(ElementName = "DutyPaymentType")]
        public string DutyPaymentType { get; set; }
        [XmlElement(ElementName = "DutyAccountNumber")]
        public string DutyAccountNumber { get; set; }
    }
}
