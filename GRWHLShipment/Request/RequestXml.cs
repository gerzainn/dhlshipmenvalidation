﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment.Request
{
    [XmlRoot(ElementName = "Request")]
    public class RequestXml
    {
        [XmlElement(ElementName = "ServiceHeader")]
        public ServiceHeaderRequest ServiceHeader { get; set; }
    }
}
