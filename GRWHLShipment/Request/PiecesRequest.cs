﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment.Request
{
    [XmlRoot(ElementName = "Pieces")]
    public class PiecesRequest
    {
        [XmlElement(ElementName = "Piece")]
        public PieceRequest Piece { get; set; }
    }
}
