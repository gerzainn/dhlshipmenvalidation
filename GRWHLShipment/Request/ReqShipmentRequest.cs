﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml.Schema;

namespace GRWHLShipment.Request
{

    [XmlRoot(ElementName = "ShipmentRequest", Namespace ="req:http://www.dhl.com" )]
    public class ReqShipmentRequest
    {
       
        [XmlElement(ElementName = "Request")]
        public RequestXml Request { get; set; }
        [XmlElement(ElementName = "RegionCode")]
        public string RegionCode { get; set; }
        [XmlElement(ElementName = "RequestedPickupTime")]
        public string RequestedPickupTime { get; set; }
        [XmlElement(ElementName = "NewShipper")]
        public string NewShipper { get; set; }
        [XmlElement(ElementName = "LanguageCode")]
        public string LanguageCode { get; set; }
        [XmlElement(ElementName = "PiecesEnabled")]
        public string PiecesEnabled { get; set; }
        [XmlElement(ElementName = "Billing")]
        public BillingRequest Billing { get; set; }
        [XmlElement(ElementName = "Consignee")]
        public ConsigneeRequest Consignee { get; set; }
        [XmlElement(ElementName = "Commodity")]
        public CommodityRequest Commodity { get; set; }
        [XmlElement(ElementName = "Dutiable")]
        public DutiableRequest Dutiable { get; set; }
        [XmlElement(ElementName = "Reference")]
        public ReferenceRequest Reference { get; set; }
        [XmlElement(ElementName = "ShipmentDetails")]
        public ShipmentDetailsRequest ShipmentDetails { get; set; }
        [XmlElement(ElementName = "Shipper")]
        public Shipper Shipper { get; set; }
        [XmlElement(ElementName = "SpecialService")]
        public List<SpecialService> SpecialService { get; set; }
        [XmlElement(ElementName = "EProcShip")]
        public string EProcShip { get; set; }
        [XmlElement(ElementName = "LabelImageFormat")]
        public string LabelImageFormat { get; set; }
       /* [XmlAttribute("req")]
        public string Req { get; set; }*/
        [XmlAttribute(Form = XmlSchemaForm.Qualified, Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string Xsi { get; set; }
        [XmlAttribute(Form = XmlSchemaForm.Qualified, Namespace = "http://www.dhl.com ship-val-global-req.xsd")]
        public string SchemaLocation { get; set; }
        [XmlAttribute(AttributeName = "schemaVersion")]
        public string SchemaVersion { get; set; }

        
    }
}
