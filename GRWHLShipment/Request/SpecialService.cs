﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment.Request
{
    [XmlRoot(ElementName = "SpecialService")]
    public class SpecialService
    {
        [XmlElement(ElementName = "SpecialServiceType")]
        public string SpecialServiceType { get; set; }
    }
}
