﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment
{
    [XmlRoot(ElementName = "Pieces")]
    public class Pieces
    {
        [XmlElement(ElementName = "Piece")]
        public Piece Piece { get; set; }
    }
}
