﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment
{
    [XmlRoot(ElementName = "ShipmentResponse")]
    public class ShipmentResponse
    {
        [XmlElement(ElementName = "AirwayBillNumber")]
        public string AirwayBillNumber { get; set; }
        [XmlElement(ElementName = "Barcodes")]
        public Barcodes Barcodes { get; set; }
        [XmlElement(ElementName = "Billing")]
        public Billing Billing { get; set; }
        [XmlElement(ElementName = "BillingCode")]
        public string BillingCode { get; set; }
        [XmlElement(ElementName = "ChargeableWeight")]
        public string ChargeableWeight { get; set; }
        [XmlElement(ElementName = "Consignee")]
        public Consignee Consignee { get; set; }
        [XmlElement(ElementName = "Contents")]
        public string Contents { get; set; }
        [XmlElement(ElementName = "CountryCode")]
        public string CountryCode { get; set; }
        [XmlElement(ElementName = "CourierMessage")]
        public string CourierMessage { get; set; }
        [XmlElement(ElementName = "CurrencyCode")]
        public string CurrencyCode { get; set; }
        [XmlElement(ElementName = "CustomerID")]
        public string CustomerID { get; set; }
        [XmlElement(ElementName = "DeliveryDateCode")]
        public string DeliveryDateCode { get; set; }
        [XmlElement(ElementName = "DeliveryTimeCode")]
        public string DeliveryTimeCode { get; set; }
        [XmlElement(ElementName = "DestinationServiceArea")]
        public DestinationServiceArea DestinationServiceArea { get; set; }
        [XmlElement(ElementName = "DHLRoutingCode")]
        public string DHLRoutingCode { get; set; }
        [XmlElement(ElementName = "DHLRoutingDataId")]
        public string DHLRoutingDataId { get; set; }
        [XmlElement(ElementName = "DimensionalWeight")]
        public string DimensionalWeight { get; set; }
        [XmlElement(ElementName = "Dutiable")]
        public Dutiable Dutiable { get; set; }
        [XmlElement(ElementName = "GlobalProductCode")]
        public string GlobalProductCode { get; set; }
        [XmlElement(ElementName = "InsuredAmount")]
        public string InsuredAmount { get; set; }
        [XmlElement(ElementName = "InternalServiceCode")]
        public List<string> InternalServiceCode { get; set; }
        [XmlElement(ElementName = "LabelImage")]
        public LabelImage LabelImage { get; set; }
        [XmlElement(ElementName = "NewShipper")]
        public string NewShipper { get; set; }
        [XmlElement(ElementName = "Note")]
        public Note Note { get; set; }
        [XmlElement(ElementName = "OriginServiceArea")]
        public OriginServiceArea OriginServiceArea { get; set; }
        [XmlElement(ElementName = "Piece")]
        public string Piece { get; set; }
        [XmlElement(ElementName = "Pieces")]
        public Pieces Pieces { get; set; }
        [XmlElement(ElementName = "ProductContentCode")]
        public string ProductContentCode { get; set; }
        [XmlElement(ElementName = "ProductShortName")]
        public string ProductShortName { get; set; }
        [XmlElement(ElementName = "Rated")]
        public string Rated { get; set; }
        [XmlElement(ElementName = "Reference")]
        public Reference Reference { get; set; }
        [XmlElement(ElementName = "RegionCode")]
        public string RegionCode { get; set; }
        [XmlAttribute(AttributeName = "res", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Res { get; set; }
        [XmlElement(ElementName = "Response")]
        public Response Response { get; set; }
        [XmlAttribute(AttributeName = "schemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string SchemaLocation { get; set; }
        [XmlElement(ElementName = "ShipmentDate")]
        public string ShipmentDate { get; set; }
        [XmlElement(ElementName = "Shipper")]
        public Shipper Shipper { get; set; }
        [XmlElement(ElementName = "SpecialService")]
        public List<SpecialService> SpecialService { get; set; }
        [XmlElement(ElementName = "WeightUnit")]
        public string WeightUnit { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
    }
}
