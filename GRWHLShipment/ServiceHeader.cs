﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment
{
    [XmlRoot(ElementName = "ServiceHeader")]
    public class ServiceHeader
    {
        [XmlElement(ElementName = "MessageReference")]
        public string MessageReference { get; set; }
        [XmlElement(ElementName = "MessageTime")]
        public string MessageTime { get; set; }
        [XmlElement(ElementName = "SiteID")]
        public string SiteID { get; set; }
    }
}
