﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment
{
    [XmlRoot(ElementName = "Billing")]
    public class Billing
    {
        [XmlElement(ElementName = "BillingAccountNumber")]
        public string BillingAccountNumber { get; set; }
        [XmlElement(ElementName = "DutyAccountNumber")]
        public string DutyAccountNumber { get; set; }
        [XmlElement(ElementName = "DutyPaymentType")]
        public string DutyPaymentType { get; set; }
        [XmlElement(ElementName = "ShipperAccountNumber")]
        public string ShipperAccountNumber { get; set; }
        [XmlElement(ElementName = "ShippingPaymentType")]
        public string ShippingPaymentType { get; set; }
    }
}
