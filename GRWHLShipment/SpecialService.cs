﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment
{
    [XmlRoot(ElementName = "SpecialService")]
    public class SpecialService
    {
        [XmlElement(ElementName = "ChargeValue")]
        public string ChargeValue { get; set; }
        [XmlElement(ElementName = "SpecialServiceDesc")]
        public string SpecialServiceDesc { get; set; }
        [XmlElement(ElementName = "SpecialServiceType")]
        public string SpecialServiceType { get; set; }
    }
}
