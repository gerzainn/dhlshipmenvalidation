﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment
{
    [XmlRoot(ElementName = "OriginServiceArea")]
    public class OriginServiceArea
    {
        [XmlElement(ElementName = "OutboundSortCode")]
        public string OutboundSortCode { get; set; }
        [XmlElement(ElementName = "ServiceAreaCode")]
        public string ServiceAreaCode { get; set; }
    }
}
