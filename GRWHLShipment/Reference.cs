﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
namespace GRWHLShipment
{
    [XmlRoot(ElementName = "Reference")]
    public class Reference
    {
        [XmlElement(ElementName = "ReferenceID")]
        public string ReferenceID { get; set; }
        [XmlElement(ElementName = "ReferenceType")]
        public string ReferenceType { get; set; }
    }
}
