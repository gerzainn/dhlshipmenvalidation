﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment
{
    [XmlRoot(ElementName = "Response")]
    public class Response
    {
        [XmlElement(ElementName = "ServiceHeader")]
        public ServiceHeader ServiceHeader { get; set; }
    }
}
