﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment
{
    [XmlRoot(ElementName = "Barcodes")]
    public class Barcodes
    {
        [XmlElement(ElementName = "AWBBarCode")]
        public string AWBBarCode { get; set; }
        [XmlElement(ElementName = "ClientIDBarCode")]
        public string ClientIDBarCode { get; set; }
        [XmlElement(ElementName = "DHLRoutingBarCode")]
        public string DHLRoutingBarCode { get; set; }
        [XmlElement(ElementName = "OriginDestnBarcode")]
        public string OriginDestnBarcode { get; set; }

    }
}
