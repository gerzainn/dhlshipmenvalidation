﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment
{
    [XmlRoot(ElementName = "Dutiable")]
    public class Dutiable
    {
        [XmlElement(ElementName = "ConsigneeEIN")]
        public string ConsigneeEIN { get; set; }
        [XmlElement(ElementName = "DeclaredCurrency")]
        public string DeclaredCurrency { get; set; }
        [XmlElement(ElementName = "DeclaredValue")]
        public string DeclaredValue { get; set; }
        [XmlElement(ElementName = "ExportLicense")]
        public string ExportLicense { get; set; }
        [XmlElement(ElementName = "ImportLicense")]
        public string ImportLicense { get; set; }
        [XmlElement(ElementName = "ScheduleB")]
        public string ScheduleB { get; set; }
        [XmlElement(ElementName = "ShipperEIN")]
        public string ShipperEIN { get; set; }
        [XmlElement(ElementName = "ShipperIDType")]
        public string ShipperIDType { get; set; }
        [XmlElement(ElementName = "TermsOfTrade")]
        public string TermsOfTrade { get; set; }
    }
}
