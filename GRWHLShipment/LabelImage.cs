﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace GRWHLShipment
{
    [XmlRoot(ElementName = "LabelImage")]
    public class LabelImage
    {
        [XmlElement(ElementName = "OutputFormat")]
        public string OutputFormat { get; set; }
        [XmlElement(ElementName = "OutputImage")]
        public string OutputImage { get; set; }
    }
}
